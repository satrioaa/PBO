package week7;

import java.util.Scanner;

class Person {
    protected String name;
    protected String address;
    protected String hobby;

    public void identity() {
        System.out.println("Nama: " + name);
        System.out.println("Alamat: " + address);
        System.out.println("Hobi: " + hobby);
    }
}

class Student extends Person {
    String nim;
    Integer spp, sks, modul;

    public String getNim() {
        return nim;
    }

    public void identity() {
        super.identity();
        System.out.println("NIM: " + nim);
    }

    public void hitungAdministrasi() {
        Integer result = spp + sks + modul;
        System.out.println("Total tagihan Administrasi mahasiswa: " + result);
    }
}

public class InheritMain {
    public static void main(String[] args) {
        Student mahasiswa = new Student();
        Scanner input = new Scanner(System.in);

        // Input data
        System.out.print("Nama: ");
        mahasiswa.name = input.nextLine();
        System.out.print("Alamat: ");
        mahasiswa.address = input.nextLine();
        System.out.print("Hobi: ");
        mahasiswa.hobby = input.nextLine();
        System.out.print("NIM: ");
        mahasiswa.nim = input.nextLine();
        System.out.print("Tagihan SPP: ");
        mahasiswa.spp = input.nextInt();
        System.out.print("Tagihan SKS: ");
        mahasiswa.sks = input.nextInt();
        System.out.print("Tagihan Modul: ");
        mahasiswa.modul = input.nextInt();

        // Output
        System.out.println("\nData Mahasiswa: ");
        mahasiswa.identity();
        mahasiswa.hitungAdministrasi();

        input.close();
    }
}
